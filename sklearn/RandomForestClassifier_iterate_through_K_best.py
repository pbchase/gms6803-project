"""
My Doc string
"""
print(__doc__)

import pandas as pd
from sklearn.feature_selection import SelectKBest, f_classif
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier
import numpy as np
from sklearn.pipeline import Pipeline
from sklearn.metrics import classification_report
from sklearn.model_selection import GridSearchCV
from sklearn.model_selection import StratifiedShuffleSplit
from sklearn.metrics import roc_auc_score
import datetime
from time import time
from sklearn.preprocessing import StandardScaler
from imblearn.under_sampling import RandomUnderSampler


def label_map(y):
    """
    Map strings in labels to integers for processing
    :param y:
    :return: int
    """
    if y=="readmitted":
       return 1
    elif y=="otherwise":
       return 0
    else:
       return 0

# time the run
start = time()

# read data
x_df = pd.DataFrame.from_csv('../dataset_diabetes/x.csv')
y_df = pd.DataFrame.from_csv('../dataset_diabetes/y.csv')

# encode categorical variables in both features and labels
x_df = pd.get_dummies(x_df)
y_df = y_df.applymap(label_map)

# Convert features into a numpy.ndarray and labels into a list
X = x_df.as_matrix()
y = y_df["readmitted"].tolist()

# Print metrics of the input data set
print "shape of the feature dataframe, x_df" + str(x_df.shape)
print "bin count of labels: " + str(np.bincount(y))

# undersample the dataset to get even-sized cohorts
rus = RandomUnderSampler(random_state=924)
X_resampled, y_resampled = rus.fit_sample(X, y)

# Split the dataset into testing and training
X_train, X_test, y_train, y_test = train_test_split(X_resampled, y_resampled, test_size=0.25, random_state=398)

# build pipeline with feature selector, scaler, and fitter
select = SelectKBest(f_classif)
rfc = RandomForestClassifier(n_jobs = 4,
                             n_estimators=500,
                             oob_score = True,
                             max_features = "auto",
                             min_samples_leaf=6)

# build pipeline
feature_selection_step = 'SelectKBest'
steps = [('standardize', StandardScaler()),
         (feature_selection_step, select),
         ('RandomForestClassifier', rfc)]
pipeline = Pipeline(steps)

# Prepare to write metrics to a file
basename = "modelmetric"
dst = '{:%Y%m%d-%H%M%S}'.format(datetime.datetime.now())
steps_label = '-'.join([x[0] for x in steps])
log_file_name = '-'.join([basename, steps_label, dst]) + ".txt"
log_file = open(log_file_name, 'w', 0)

roc_auc_by_k = dict()

for select_k_best_k in np.arange(1,51,1):
    loop_start_time = time()
    # set grid search params
    parameters = dict(SelectKBest__k=[select_k_best_k],
                      RandomForestClassifier__min_samples_leaf=[2,7,12])
    cv_strategy = StratifiedShuffleSplit(n_splits=10, test_size=0.1, random_state=839)
    cv = GridSearchCV(pipeline, param_grid=parameters, scoring="roc_auc", n_jobs=4, cv=cv_strategy)

    # fit and predict labels
    cv.fit(X_train, y_train)
    y_predictions = cv.predict(X_test)

    # compare predictions to know labels
    report = classification_report(y_test, y_predictions)

    final_pipeline = cv.best_estimator_
    select_indices = final_pipeline.named_steps[feature_selection_step].transform(
        np.arange(len(x_df.columns)).reshape(1, -1)
    )
    feature_names = x_df.columns[select_indices]

    roc_auc_for_test = roc_auc_score(y_test, y_predictions)
    roc_auc_by_k[select_k_best_k] = roc_auc_for_test

    loop_time = time() - loop_start_time

    log_file.write("select_k_best_k:" + str(select_k_best_k) + "\n")
    log_file.write("roc_auc_score:" + str(roc_auc_for_test) + "\n")
    log_file.write("best_params:" + str(cv.best_params_) + "\n")
    log_file.write("loop_time:" + str(loop_time) + "\n")
    log_file.write("feature_names:" + str(feature_names) + "\n")
    log_file.write("report:\n")
    log_file.write(report)
    log_file.write("\n")

    # and print the report
    print("-----------------------------------")
    print("select_k_best_k:" + str(select_k_best_k))
    print("roc_auc_score:" + str(roc_auc_for_test))
    print("best_params:" + str(cv.best_params_))
    print("loop_time:" + str(loop_time))
    print("\n feature_names:")
    print(feature_names)
    print("report:")
    print(report)
    print("")

# record run time
run_time = time() - start

print("\n roc_auc_by_k:")
print(roc_auc_by_k)
print("run_time:" + str(run_time) + "\n")

log_file.write("roc_auc_by_k:" + str(roc_auc_by_k))
log_file.write("run_time:" + str(run_time) + "\n")
