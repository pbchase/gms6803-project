"""
My Doc string
"""
print(__doc__)

import pandas as pd
from sklearn.feature_selection import SelectPercentile, f_classif
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier
import numpy as np
from sklearn.pipeline import Pipeline
from sklearn.metrics import classification_report
from sklearn.model_selection import GridSearchCV
from sklearn.model_selection import StratifiedShuffleSplit
from sklearn.metrics import roc_auc_score
import datetime
from sklearn.preprocessing import StandardScaler
from imblearn.under_sampling import RandomUnderSampler
import matplotlib.pyplot as plt
from scikitplot import estimators, metrics


def label_map(y):
    """
    Map strings in labels to integers for processing
    :param y:
    :return: int
    """
    if y == "readmitted":
        return 1
    elif y == "otherwise":
        return 0
    else:
        return 0


# read data
x_df = pd.DataFrame.from_csv('../dataset_diabetes/x.csv')
y_df = pd.DataFrame.from_csv('../dataset_diabetes/y.csv')

# encode categorical variables in both features and labels
x_df = pd.get_dummies(x_df)
y_df = y_df.applymap(label_map)

# Convert features into a numpy.ndarray and labels into a list
X = x_df.as_matrix()
y = y_df["readmitted"].tolist()

# Print metrics of the input data set
print "shape of the feature dataframe, x_df" + str(x_df.shape)
print "bin count of labels: " + str(np.bincount(y))

# standardize X and y
scaler = StandardScaler()
X_rescaled = scaler.fit_transform(X)

# undersample the dataset to get even-sized cohorts
rus = RandomUnderSampler(random_state=924)
X_resampled, y_resampled = rus.fit_sample(X_rescaled, y)

# Split the dataset into testing and training
X_train, X_test, y_train, y_test = train_test_split(X_resampled, y_resampled, test_size=0.25, random_state=398)

# build pipeline with feature selector, scaler, and fitter
select = SelectPercentile(f_classif)
rfc = RandomForestClassifier(n_jobs=4)

# build pipeline
feature_selection_step = 'SelectPercentileFClassif'
steps = [(feature_selection_step, select),
         ('RandomForestClassifier', rfc)]
pipeline = Pipeline(steps)

# set grid search params
sp_percentiles = [20]
parameters = dict(SelectPercentileFClassif__percentile=sp_percentiles,
                  RandomForestClassifier__n_estimators=[1000],
                  RandomForestClassifier__oob_score = [True],
                  RandomForestClassifier__max_features = ["auto"],
                  RandomForestClassifier__min_samples_leaf=[2,6,12])
cv_strategy = StratifiedShuffleSplit(n_splits=10, test_size=0.1, random_state=839)
cv = GridSearchCV(pipeline, param_grid=parameters, scoring="roc_auc", n_jobs=4, cv=cv_strategy)

# fit and predict labels
cv.fit(X_train, y_train)
y_predictions = cv.predict(X_test)

# compare predictions to know labels
report = classification_report(y_test, y_predictions)

final_pipeline = cv.best_estimator_
selector = final_pipeline.named_steps[feature_selection_step]
selected_indices = selector.transform(
    np.arange(len(x_df.columns)).reshape(1, -1)
)
selected_names = x_df.columns[selected_indices]


# write metrics to a file
basename = "modelmetric"
dst = '{:%Y%m%d-%H%M%S}'.format(datetime.datetime.now())
steps_label = '-'.join([x[0] for x in steps])
log_file_name = '-'.join([basename, steps_label, dst]) + ".txt"
log_file = open(log_file_name, 'w')

# write step labels in a easy-to-read format
i = 1
for (my_step, dummy) in steps:
    log_file.write("step_" + str(i) + ":" + str(my_step) + "\n")
    i += 1

log_file.write(
    "shape of the feature dataframe, x_df after one hot encoding and before down sampling:" + str(x_df.shape) + "\n")
log_file.write("roc_auc_score:" + str(roc_auc_score(y_test, y_predictions)) + "\n")
log_file.write("best_estimator:" + str(cv.best_estimator_) + "\n")
log_file.write("best_params:" + str(cv.best_params_) + "\n")
log_file.write("cv_results:" + str(cv.cv_results_) + "\n")
log_file.write("feature_names:" + str(selected_names) + "\n")
log_file.write("n_splits:" + str(cv.n_splits_) + "\n")
log_file.write("\nreport:\n" + report + "\n")

# and print the report
print("\n cv_results_:")
print cv.cv_results_
print("\n selected_names:")
print(selected_names)
print("\n report:")
print(report)
print("best_params:" + str(cv.best_params_))
print("roc_auc_score:" + str(roc_auc_score(y_test, y_predictions)))

# plot feature_importances
best_classifier = cv.best_estimator_.named_steps['RandomForestClassifier']
print "\nfeature_importances:"
print best_classifier.feature_importances_
plt.figure(1)
estimators.plot_feature_importances(best_classifier, feature_names=None, max_num_features=25,
                                    order='descending', x_tick_rotation=0, ax=None, figsize=None,
                                    title_fontsize='large', text_fontsize='medium')
plt.show()

# plot ROC_AUC
y_probabilities = cv.predict_proba(X_test)
plt.figure(2)
metrics.plot_roc_curve(y_test, y_probabilities)
plt.show()
