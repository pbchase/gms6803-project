"""
===================================================
Recursive feature elimination with cross-validation
===================================================

A recursive feature elimination example with automatic tuning of the
number of features selected with cross-validation.
"""
print(__doc__)

import matplotlib.pyplot as plt
import pandas as pd
from sklearn.preprocessing import LabelEncoder
from collections import defaultdict
from sklearn.svm import LinearSVC
from sklearn.linear_model import LogisticRegression
from sklearn.feature_selection import SelectFromModel

# read data
f = open('../dataset_diabetes/x.csv')
x_df = pd.read_csv(f, header = 0)

f = open('../dataset_diabetes/y.csv')
y_df = pd.read_csv(f, header = 0)

# encode the many categorical variables in the features and the labels
d_X = defaultdict(LabelEncoder)
x_df_encoded = x_df.apply(lambda x: d_X[x.name].fit_transform(x))
# make an array of arrays for the features
X = x_df_encoded.as_matrix()

# make y by encoding the y dataframe
y_le = LabelEncoder()
y = y_le.fit_transform(y_df["readmitted"])

print X.shape
lr = LogisticRegression().fit(X, y)
model = SelectFromModel(lr, prefit=True)
print model.get_support()
print model.get_support(indices=True)
X_new = model.transform(X)
print X_new.shape

