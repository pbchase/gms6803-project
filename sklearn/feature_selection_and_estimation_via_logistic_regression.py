"""
===================================================
Recursive feature elimination with cross-validation
===================================================

A recursive feature elimination example with automatic tuning of the
number of features selected with cross-validation.
"""
print(__doc__)

import matplotlib.pyplot as plt
import pandas as pd
from sklearn.preprocessing import LabelEncoder
from collections import defaultdict
from sklearn.svm import LinearSVC, SVC
from sklearn.linear_model import LogisticRegression
from sklearn.feature_selection import SelectFromModel
from sklearn.model_selection import train_test_split
from sklearn.model_selection import cross_val_score
from sklearn import metrics
import numpy as np

# read data
f = open('../dataset_diabetes/x.csv')
x_df = pd.read_csv(f, header = 0)

f = open('../dataset_diabetes/y.csv')
y_df = pd.read_csv(f, header = 0)

# encode the many categorical variables in the features and the labels
d_X = defaultdict(LabelEncoder)
x_df_encoded = x_df.apply(lambda x: d_X[x.name].fit_transform(x))
# make an array of arrays for the features
X = x_df_encoded.as_matrix()
print "Shape of data set as read: "
print X.shape

# make y by encoding the y dataframe
y_le = LabelEncoder()
y = y_le.fit_transform(y_df["readmitted"])
print "bin count of labels: "
print np.bincount(y)

# Split the dataset into testing and training
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.25, random_state=0)

lr = LogisticRegression().fit(X_train, y_train)
model = SelectFromModel(lr, prefit=True)
print "Selected features as booleans:"
print model.get_support()
print "Selected features as indices:"
print model.get_support(indices=True)
X_train_new = model.transform(X_train)
X_test_new = model.transform(X_test)
print "Shape of training data: "
print X_train_new.shape

lr_model = LogisticRegression()
scores = cross_val_score(lr_model, X_train_new, y_train, cv=10, scoring='accuracy')
print "Cross-validation scores: "
print scores
print "Mean of cross-validation scores: "
print scores.mean()
# model.fit(X_train_new, y_train)

# predicted = model.predict(X_test_new)
# print predicted
#
# # generate evaluation metrics
# print metrics.accuracy_score(y_test, predicted)
