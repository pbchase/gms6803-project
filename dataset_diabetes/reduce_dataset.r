library(RWeka)
library(dplyr)

cleaned_data <- read.arff("diabetes_data.arff")

reduced_data_set <- cleaned_data
#%>%  sample_n(64000)

X <- reduced_data_set %>%
  select(-one_of("readmitted"))
y <- reduced_data_set %>%
  select(one_of("readmitted"))

write.csv(X, 'x.csv')
write.csv(y, 'y.csv')
