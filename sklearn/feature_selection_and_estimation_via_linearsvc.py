"""
===================================================
Recursive feature elimination with cross-validation
===================================================

A recursive feature elimination example with automatic tuning of the
number of features selected with cross-validation.
"""
print(__doc__)

import matplotlib.pyplot as plt
import pandas as pd
from sklearn.preprocessing import LabelEncoder
from collections import defaultdict
from sklearn.svm import LinearSVC, SVC
from sklearn.linear_model import LogisticRegression
from sklearn.feature_selection import SelectFromModel
from sklearn.model_selection import train_test_split
from sklearn.model_selection import cross_val_score
from sklearn import metrics
import numpy as np


def label_map(y):
    """
    Map strings in labels to integers for processing
    :param y:
    :return: int
    """
    if y=="readmitted":
       return 1
    elif y=="otherwise":
       return 0
    else:
       return 0

# read data
x_df = pd.DataFrame.from_csv('../dataset_diabetes/x.csv')
y_df = pd.DataFrame.from_csv('../dataset_diabetes/y.csv')

# encode categorical variables in both features and labels
x_df = pd.get_dummies(x_df)
y_df = y_df.applymap(label_map)

# Convert features into a numpy.ndarray and labels into a list
X = x_df.as_matrix()
y = y_df["readmitted"].tolist()

print "bin count of labels: "
print np.bincount(y)

# Split the dataset into testing and training
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.25, random_state=0)

lsvc = LinearSVC(C=0.01, penalty="l1", dual=False).fit(X_train, y_train)
model = SelectFromModel(lsvc, prefit=True)
print "Selected features as indices:"
print model.get_support(indices=True)
print "Selected feature names:"
print np.array(list(x_df))[model.get_support(indices=True)].tolist()

X_train_new = model.transform(X_train)
X_test_new = model.transform(X_test)
print "Shape of training data: "
print X_train_new.shape

estimator_model = LinearSVC()
scores = cross_val_score(estimator_model, X_train_new, y_train, cv=10, scoring='accuracy')
print "estimator: linearSVC"
print "Cross-validation scores: "
print scores
print "Mean of cross-validation scores: "
print scores.mean()


# model.fit(X_train_new, y_train)

# predicted = model.predict(X_test_new)
# print predicted
#
# # generate evaluation metrics
# print metrics.accuracy_score(y_test, predicted)
