setwd("~/classes/gms6803-project/dataset_diabetes")
colClasses = c(
  "integer", "integer", "factor", "factor", "factor", "factor",
  "integer", "integer", "integer", "integer", "factor", "factor",
  "integer", "integer", "integer", "integer", "integer", "integer",
  "character", "character", "character", "integer", "factor", "factor",
  "factor", "factor", "factor", "factor", "factor", "factor",
  "factor", "factor", "factor", "factor", "factor", "factor",
  "factor", "factor", "factor", "factor", "factor", "factor",
  "factor", "factor", "factor", "factor", "factor", "factor",
  "factor", "factor" )
diabetes<- read.csv("diabetic_data.csv", colClasses = colClasses)

str(diabetes)
levels(diabetes$payer_code)

# Remove the all but the first encounter for patients who had multiple encounters.
duplicated_rows <- duplicated(diabetes$patient_nbr)
sans_dups <- diabetes[!c(duplicated_rows),]

# How many patients had duplicate records?
ids_of_duplicated_patients <- unique(diabetes[c(duplicated_rows), "patient_nbr"])
length(ids_of_duplicated_patients)

# Remove patients who discharge to Hospice or death
# All possible_death_or_hospice_dispositions <- c(11,13,14,19,20,21)
death_or_hospice_dispositions <- c(11,13,14,19,20,21)
# How many of each type of discharge_disposition_id exist?
library(dplyr)
discharge_counts <- sans_dups %>% select(discharge_disposition_id) %>%
  filter(discharge_disposition_id %in% death_or_hospice_dispositions) %>%
  group_by(discharge_disposition_id) %>% summarise(count=n())

# remove the dead or dying from our dataset
sans_dead_or_dying <- sans_dups[!(sans_dups$discharge_disposition_id %in% death_or_hospice_dispositions), ]

# how many patients remain?
# target is 69,984
nrow(sans_dead_or_dying)

# how many do we still need to remove?
nrow(sans_dead_or_dying) - 69984

# convert ids from numeric to factors
admission_types <- read.csv("admission_type.csv", colClasses = c("integer", "character"))
admission_sources <- read.csv("admission_source.csv", colClasses = c("integer", "character"))
discharge_dispositions <- read.csv("discharge_disposition.csv", colClasses = c("integer", "character"))
sans_dead_or_dying <- sans_dead_or_dying %>%
  left_join(admission_types, c("admission_type_id", "admission_type_id")) %>%
  left_join(admission_sources, c("admission_source_id", "admission_source_id")) %>%
  left_join(discharge_dispositions, c("discharge_disposition_id", "discharge_disposition_id")) %>%
  mutate(admission_type = as.factor(admission_type)) %>%
  mutate(admission_source = as.factor(admission_source)) %>%
  mutate(discharge_disposition = as.factor(discharge_disposition))

# Assess data sparseness
unknowns <- function(x,rows){sum(x=='?')/rows}
portion_unknown <- apply(sans_dead_or_dying, 2, unknowns, nrow(sans_dead_or_dying))
portion_unknown[portion_unknown > 0]

# remove sparse columns
columns_to_drop <- c("weight", "payer_code")
sans_sparse_columns <-
  sans_dead_or_dying[, !(names(sans_dead_or_dying) %in% columns_to_drop)]

# adjust levels for medical specialty
sans_sparse_columns$medical_specialty <-
  recode(sans_sparse_columns$medical_specialty, "?" = "missing",
         .default=levels(sans_sparse_columns$medical_specialty))

# Change dx codes for two ICD9 codes that were not described in the paper, but appear to have
# been classified as "Other symptoms, signs, and ill-defined conditions" in the paper.
# Together, these two codes appear 381 times in diag_1.
sans_sparse_columns[sans_sparse_columns$diag_1 %in% c("783", "789"),c('diag_1')] <- c("799")
sans_sparse_columns[sans_sparse_columns$diag_2 %in% c("783", "789"),c('diag_2')] <- c("799")
sans_sparse_columns[sans_sparse_columns$diag_3 %in% c("783", "789"),c('diag_3')] <- c("799")

# merge diagnosis groups
diagnosis_groups <- read.csv("diagnoses_by_icd9.csv", colClasses = c("integer", "character", "character"))
with_diagnosis_groups <- sans_sparse_columns %>%
  left_join(diagnosis_groups, by=c("diag_1" = "codes")) %>%
  dplyr::rename(diag_1_group = diagnosis) %>%
  mutate(diag_1_group = factor(diag_1_group))

summary_by_dx_group <- with_diagnosis_groups %>%
  group_by(rank_in_paper) %>%
  summarise(qty = n()) %>%
  arrange(rank_in_paper)

# compare diagnosis grouping with that from the paper
summary_dx_grouping_from_paper <-
  read.csv("disease_group_qty.csv", colClasses = c("integer", "integer", "character"))

options(digits=2)
combined_dx_grouping_comparison <- summary_by_dx_group %>%
  left_join(summary_dx_grouping_from_paper, by="rank_in_paper") %>%
  mutate(ratio = (qty.y-qty.x)/qty.y, delta = qty.y-qty.x)

comparison_of_totals <- combined_dx_grouping_comparison %>%
  summarise(total.y = sum(qty.y, na.rm = TRUE), total.x = sum(qty.x, na.rm = TRUE)) %>%
  mutate(ratio = (total.y-total.x)/total.y, delta=total.y-total.x)

combined_dx_grouping_comparison

# (re)create diagnosis grouping for dx 1, 2 and 3
diagnosis_groups <- read.csv("diagnoses_by_icd9_abbreviated.csv", colClasses = c("integer", "character", "character"))
with_diagnosis_groups <- sans_sparse_columns %>%
  left_join(diagnosis_groups, by=c("diag_1" = "codes")) %>%
  dplyr::rename(diag_1_group = diagnosis) %>%
  mutate(diag_1_group = factor(diag_1_group))

with_diagnosis_groups <- with_diagnosis_groups %>%
  left_join(diagnosis_groups, by=c("diag_2" = "codes")) %>%
  dplyr::rename(diag_2_group = diagnosis) %>%
  mutate(diag_2_group = factor(diag_2_group))

with_diagnosis_groups <- with_diagnosis_groups %>%
  left_join(diagnosis_groups, by=c("diag_3" = "codes")) %>%
  dplyr::rename(diag_3_group = diagnosis) %>%
  mutate(diag_3_group = factor(diag_3_group))

# check the frequency in the collapsed dx groups
with_diagnosis_groups %>%
  group_by(rank_in_paper) %>%
  summarise(qty = n()) %>%
  arrange(rank_in_paper)

with_diagnosis_groups %>%
  group_by(diag_1_group) %>%
  summarise(qty = n()) %>%
  arrange(desc(qty))

# Collapse readmitted variables into two levels "readmitted" and "otherwise"
# where "otherwise" is both ">30" and "NO" as is done in the paper
levels(with_diagnosis_groups$readmitted) <- c("readmitted", "otherwise", "otherwise")


# Remove redundant columns and move dependent var--readmitted--to the end
columns_to_drop <- c("rank_in_paper", "diag_1", "diag_2", "diag_3",
                     "encounter_id", "patient_nbr", "rank_in_paper.x",
                     "rank_in_paper.y", "admission_type_id",
                     "admission_source_id", "discharge_disposition_id")

dependent_var_name <- c("readmitted")
cleaned_data <-
  with_diagnosis_groups[,
    c(setdiff(names(with_diagnosis_groups),
              dependent_var_name), dependent_var_name)] %>%
  select(-one_of(columns_to_drop))

# write an arff file use with WEKA
library(RWeka)
write.arff(cleaned_data, "diabetes_data.arff")

# Write independent variables removing redundant variables related to diagnosis.
independent_vars <- cleaned_data %>%
  select(-one_of(dependent_var_name))
write.csv(independent_vars, "x.csv")
save(independent_vars, "x.rdata")

# Write dependent variable
dependent_var <- cleaned_data %>% select(one_of(dependent_var_name))
write.csv(dependent_var, "y.csv")

# write out count by medical specialities
cleaned_data %>%
  group_by(medical_specialty) %>%
  summarise(qty= n()) %>%
  arrange(desc(qty)) %>%
  write.csv("medical_specialties.csv")

# create datasets with the significant hba1c-related variables from the strack et al paper
features_from_strack_et_al <- c("A1Cresult", "change", "diag_1_group")

# Write independent variables removing redundant variables related to diagnosis.
independent_vars <- cleaned_data %>%
  select(-one_of(dependent_var_name)) %>%
  select(features_from_strack_et_al)
write.csv(independent_vars, "x_strack_et_al.csv")
