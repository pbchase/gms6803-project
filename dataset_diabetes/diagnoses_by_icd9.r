# Build a dataset that will map icd9 codes to groups of diagnoses

# Write a function to append a set of icd9 codes and a grouping to an existing dataframe
add_diagnoses_to_grouping <- function(rank, codes, mygroup, mydata) {
  rank_in_paper <- rep(rank,length(codes))
  diagnosis <- rep(mygroup,length(codes))
  temp <- data.frame(rank_in_paper, diagnosis, codes)
  mydata <- rbind(mydata, temp)
  return(mydata)
}

# build a mapping of ICD9 codes to body-system diagnoses
# circulatory
codes <- c(seq(from=390, to=459), 785)
diagnosis <- rep('Diseases of the circulatory system',length(codes))
rank_in_paper <- rep(1,length(codes))
diagnoses_by_icd9 <- data.frame(rank_in_paper, diagnosis, codes)

# respiratory
codes <- c(seq(from=460, to=519), 786)
diagnoses_by_icd9 <- add_diagnoses_to_grouping(2, codes, "Diseases of the respiratory system", diagnoses_by_icd9)

# digestive
codes <- c(seq(from=520, to=579), 787)
diagnoses_by_icd9 <- add_diagnoses_to_grouping(3, codes, "Diseases of the digestive system", diagnoses_by_icd9)

# diabetes - unpadded
codes <- as.character(c(seq(from=1, to=99)))
codes_up <- paste('250', codes, sep='.')
# diabetes - padded
codes <- as.character(sprintf("%02s", seq(from=1, to=9)))
codes_p <- paste('250', codes, sep='.')
# other diabetes
codes <- c(250)
diagnoses_by_icd9 <- add_diagnoses_to_grouping(4, c(codes_up, codes_p, codes), "Diabetes mellitus", diagnoses_by_icd9)

# injury
codes <- c(seq(from=800, to=999))
diagnoses_by_icd9 <- add_diagnoses_to_grouping(5, codes, "Injury and poisoning", diagnoses_by_icd9)

# musculoskeletal
codes <- c(seq(from=710, to=739))
diagnoses_by_icd9 <- add_diagnoses_to_grouping(6, codes, "Diseases of the musculoskeletal system and connective tissue", diagnoses_by_icd9)

# genitourinary
codes <- c(seq(from=580, to=629), 788)
diagnoses_by_icd9 <- add_diagnoses_to_grouping(7, codes, "Diseases of the genitourinary system", diagnoses_by_icd9)

# neoplasms
codes <- c(seq(from=140, to=239))
diagnoses_by_icd9 <- add_diagnoses_to_grouping(8, codes, "Neoplasms", diagnoses_by_icd9)

# other diagnoses
codes <- c(780, 781, 784, seq(from=790, to=799), "?")
diagnoses_by_icd9 <- add_diagnoses_to_grouping(9, codes, "Other symptoms, signs, and ill-defined conditions", diagnoses_by_icd9)

codes <- c(seq(from=240, to=249), seq(from=251, to=279))
diagnoses_by_icd9 <- add_diagnoses_to_grouping(10, codes, "Endocrine, nutritional, and metabolic diseases and immunity disorders, without diabetes", diagnoses_by_icd9)

codes <- c(seq(from=680, to=709), 782)
diagnoses_by_icd9 <- add_diagnoses_to_grouping(11, codes, "Diseases of the skin and subcutaneous tissue", diagnoses_by_icd9)

codes <- c(seq(from=1, to=139))
diagnoses_by_icd9 <- add_diagnoses_to_grouping(12, codes, "Infectious and parasitic diseases", diagnoses_by_icd9)

codes <- c(seq(from=290, to=319))
diagnoses_by_icd9 <- add_diagnoses_to_grouping(13, codes, "Mental disorders", diagnoses_by_icd9)

# Other - prefixed with 'E'
codes <- as.character(c(seq(from=812, to=987)))
codes <- paste('E', codes, sep='')
diagnoses_by_icd9 <- add_diagnoses_to_grouping(14, codes, "External causes of injury and supplemental classication", diagnoses_by_icd9)

# Other - prefixed with 'V', two digits, zero padded
codes <- as.character(sprintf("%02s", seq(from=1, to=99)))
codes <- paste('V', codes, sep='')
diagnoses_by_icd9 <- add_diagnoses_to_grouping(14, codes, "External causes of injury and supplemental classication", diagnoses_by_icd9)

codes <- c(seq(from=280, to=289))
diagnoses_by_icd9 <- add_diagnoses_to_grouping(15, codes, "Diseases of the blood and blood-forming organs", diagnoses_by_icd9)

codes <- c(seq(from=320, to=359))
diagnoses_by_icd9 <- add_diagnoses_to_grouping(16, codes, "Diseases of the nervous system", diagnoses_by_icd9)


codes <- c(seq(from=630, to=679))
diagnoses_by_icd9 <- add_diagnoses_to_grouping(17, codes, "Complications of pregnancy, childbirth, and the puerperium, etc.", diagnoses_by_icd9)

codes <- c(seq(from=360, to=389), "365.44")
diagnoses_by_icd9 <- add_diagnoses_to_grouping(18, codes, "Diseases of the sense organs", diagnoses_by_icd9)

codes <- c(seq(from=740, to=759))
diagnoses_by_icd9 <- add_diagnoses_to_grouping(19, codes, "Congenital anomalies", diagnoses_by_icd9)


# Save the result
write.csv(diagnoses_by_icd9, file = "diagnoses_by_icd9.csv", row.names = FALSE)

# write mapping file that summarizes ranks 9 - 19 into a single rank-9 "Other" category.
diagnoses_by_icd9_abbreviated <- diagnoses_by_icd9 %>%
  mutate(diagnosis = as.character(diagnosis)) %>%
  mutate(rank_in_paper = if_else(diagnoses_by_icd9$rank_in_paper %in% seq(9,19),
                               as.integer(9), as.integer(diagnoses_by_icd9$rank_in_paper))) %>%
  mutate(diagnosis = if_else(diagnoses_by_icd9$rank_in_paper %in% seq(9,19),
                                "Other", as.character(diagnoses_by_icd9$diagnosis))) %>%
  mutate(diagnosis = factor(diagnosis))

write.csv(diagnoses_by_icd9_abbreviated, file = "diagnoses_by_icd9_abbreviated.csv", row.names = FALSE)
