"""
My Doc string
"""
print(__doc__)

import pandas as pd
from sklearn.feature_selection import SelectPercentile, f_classif
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LogisticRegression
import numpy as np
from sklearn.pipeline import Pipeline
from sklearn.model_selection import GridSearchCV
from sklearn.model_selection import StratifiedShuffleSplit
from sklearn.preprocessing import StandardScaler
from imblearn.under_sampling import RandomUnderSampler


def label_map(y):
    """
    Map strings in labels to integers for processing
    :param y:
    :return: int
    """
    if y=="readmitted":
       return 1
    elif y=="otherwise":
       return 0
    else:
       return 0

# read data
x_df = pd.DataFrame.from_csv('../dataset_diabetes/x.csv')
y_df = pd.DataFrame.from_csv('../dataset_diabetes/y.csv')

# encode categorical variables in both features and labels
x_df = pd.get_dummies(x_df)
y_df = y_df.applymap(label_map)

# Convert features into a numpy.ndarray and labels into a list
X = x_df.as_matrix()
y = y_df["readmitted"].tolist()

# Print metrics of the input data set
print "shape of the feature dataframe, x_df" + str(x_df.shape)
print "bin count of labels: " + str(np.bincount(y))

# undersample the dataset to get even-sized cohorts
rus = RandomUnderSampler(random_state=924)
X_resampled, y_resampled = rus.fit_sample(X, y)

# Split the dataset into testing and training
X_train, X_test, y_train, y_test = train_test_split(X_resampled, y_resampled, test_size=0.25, random_state=398)

# build pipeline with feature selector, scaler, and fitter
select = SelectPercentile(f_classif)
lr = LogisticRegression(dual=False, n_jobs=4)

# build pipeline
feature_selection_step = 'SelectPercentileFClassif'
steps = [('standardize', StandardScaler()),
         (feature_selection_step, select),
         ('LogisticRegression', lr)]
pipeline = Pipeline(steps)

# set grid search params
sp_percentiles = [10]
parameters = dict(SelectPercentileFClassif__percentile=sp_percentiles,
                  LogisticRegression__penalty=["l1"],
                  LogisticRegression__C=[0.05])
cv_strategy = StratifiedShuffleSplit(n_splits=10, test_size=0.1, random_state=839)
cv = GridSearchCV(pipeline, param_grid=parameters, scoring="roc_auc", n_jobs=4, cv=cv_strategy)

# fit and predict labels
cv.fit(X_train, y_train)

final_pipeline = cv.best_estimator_
select_indices = final_pipeline.named_steps[feature_selection_step].transform(
    np.arange(len(x_df.columns)).reshape(1, -1)
)
feature_names = x_df.columns[select_indices]

for item in feature_names:
    for feature in item:
        print feature
