"""
===================================================
Recursive feature elimination with cross-validation
===================================================

A recursive feature elimination example with automatic tuning of the
number of features selected with cross-validation.
"""
print(__doc__)

import matplotlib.pyplot as plt
from sklearn.svm import SVC
from sklearn.model_selection import StratifiedKFold
from sklearn.feature_selection import RFECV
from sklearn.datasets import make_classification
# import numpy as np
import pandas as pd
from sklearn.preprocessing import LabelEncoder
from collections import defaultdict

# read data
f = open('../dataset_diabetes/x.csv')
x_df = pd.read_csv(f, header = 0)

f = open('../dataset_diabetes/y.csv')
y_df = pd.read_csv(f, header = 0)

# encode the many categorical variables in the features and the labels
d_X = defaultdict(LabelEncoder)
x_df_encoded = x_df.apply(lambda x: d_X[x.name].fit_transform(x))
# make an array of arrays for the features
X = x_df_encoded.as_matrix()

# make y by encoding the y dataframe
y_le = LabelEncoder()
y = y_le.fit_transform(y_df["readmitted"])

print X
print y

# Create the RFE object and compute a cross-validated score.
svc = SVC(kernel="linear")
# The "accuracy" scoring is proportional to the number of correct
# classifications
rfecv = RFECV(estimator=svc, step=1, cv=StratifiedKFold(2),
              scoring='accuracy')
rfecv.fit(X, y)

print("Optimal number of features : %d" % rfecv.n_features_)

print("Ranked features are :")
print rfecv.ranking_

# Plot number of features VS. cross-validation scores
plt.figure()
plt.xlabel("Number of features selected")
plt.ylabel("Cross validation score (nb of correct classifications)")
plt.plot(range(1, len(rfecv.grid_scores_) + 1), rfecv.grid_scores_)
plt.show()
